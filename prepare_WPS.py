#!/usr/bin/env python

import os, shutil, subprocess
import os.path
import stat
import string
import sys
import copy
import WRF_LIS_lib
                        
def prepare_WPS(input_dic):
    # Make sure INI_FORMAT is given in lower case
    input_dic['INI_FORMAT'] = (input_dic['INI_FORMAT']).lower()

    # Add COUPLED = integer form of coupled
    input_dic['COUPLED'] = int(input_dic['coupled'])

    #-----------------------------------------------------------------------------------
    # Read the grid info from the geo_em.d01.nc file first.
    GEOpath = input_dic['GEO_PATH'] + '/'
    gridtype = input_dic['gridtype']

    # Read grid info for WRF
    tmp = WRF_LIS_lib.get_grid('WRF', gridtype, "{GEO_PATH}".format(**input_dic), input_dic['max_dom'])
    input_dic.update(tmp)

    # A few initialisations
    days_in_month = (31,28,31,30,31,30,31,31,30,31,30,31)

    year        = input_dic['start_year']
    month       = input_dic['start_month']
    end_year    = input_dic['end_year']
    end_month   = input_dic['end_month']

    # If we start from scratch with MK3.5 we need to process previous month
    if ((input_dic['INI_FORMAT'] == "mk35") and input_dic['first'] ):
        if (month == 1):
            month = 12
            year = year - 1
        else:
            month = month - 1


    # Get a new dictionary for the changes (cosmetic)
    changes = input_dic

    # Save first year, month and day
    changes["FYEAR"]  = '{:0>4}'.format(year)
    changes["FMONTH"] = '{:0>2}'.format(month)
    changes["FDAY"]   = "01"

    changes['SYEAR']  = changes["FYEAR"]
    changes['SMONTH'] = changes["FMONTH"]

    #-----------------------------------------------------------------------------------
    # Add some values
    changes['ISRESTART'] = '.false.'
    changes['RES_WRF'] = 1440
    changes['RUN_DIR'] = os.getcwd()
    changes['FIRST'] = int(input_dic['first'])
    changes['NOSECOND'] = changes['FIRST']
    changes['NOFIRST'] = str(1-changes['FIRST'])
    tmp = changes['BASENAMES'].split()
    changes['BASENAMES_NAMELIST'] = ("'"+base+"'" for base in tmp)
    changes['BASENAMES_NAMELIST'] = ', '.join(changes['BASENAMES_NAMELIST'])
    changes['BASENAMES_CLEANUP']  = ''.join([t+'* ' for t in tmp])

    # Create dependency list for metgrid. If running coupled, do not run
    # geogrid. Run geogrid if uncoupled.
    # Choose overlap value too.
    dependencies = ['namelist.wps','.geogrid.done','.ungrib.done']
    if changes['coupled']:
        changes['OLAP'] = int(float(changes['WRF_ts'])/60.)
        changes['dependencies'] = dependencies[0]+' '+dependencies[2]
    else:
        changes['OLAP'] = 00
        changes['dependencies'] = ' '.join(dependencies)


    # Copy post-processing code to deck directory and symlink to run directory
    shutil.copy("post-process-WPS.py",input_dic['DECK_PATH'])
    WRF_LIS_lib.create_symlink(os.path.join(input_dic['DECK_PATH'],"post-process-WPS.py"),input_dic['COUPLED_PATH'])

    # Copy the run directory input data from WRFV3/test_real into 'COUPLED_PATH'
    subprocess.call(["rsync","-rL", "--exclude=*.exe", os.path.join(input_dic["CODEDIR_ROOT"],"WRFV3","run","."), input_dic['COUPLED_PATH']])

    # If initial format is Grib, update the Makefile
    fin = open('../templates/Makefile', 'r')
    fout = open('{DECK_PATH}/Makefile'.format(**changes),'w')

    # Gives permissions: -rwxr--r-- to output file
    os.chmod(fout.name,stat.S_IRWXU)

    # Loop over the lines of the template csh script.
    for line in fin:
        line = string.Template(line).safe_substitute(changes)

        fout.write(line)

    # End of loop on the lines of the template csh script
    # Close files
    fout.close()
    fin.close()

    ## Symlink to run directory
    if changes['FIRST']:
        WRF_LIS_lib.create_symlink(os.path.join(changes['DECK_PATH'],'Makefile'),input_dic['COUPLED_PATH'])

    #set up loop to be over all months in the years mentioned
    while (year < end_year or (year == end_year and month <= end_month)):

        changes['SDAY'] = 1
        changes['NDAY'] = 1

        # Is it February of a leap year?
        changes['LEAP_YEAR'] = int(( month==2 and ( ( (year%4==0) and (year%100 !=0) ) or year%400 == 0 ) ))

        print("Prepare year/month: {0}/{1}".format(year, month))

        # Get previous month and previous year
        tmp=WRF_LIS_lib.set_pndate(year,month,changes['WRF_nmonths'],changes)
        changes.update(tmp)

        changes['SDAY']='{:0>2}'.format(changes['SDAY'])
        changes['NDAY']='{:0>2}'.format(changes['NDAY'])

        # Get the year in the history files
        changes['HYEAR'] = str(year-1700)

        # build the values of all flags
        changes['PREV_BDY'] = 'wrfbdy_d01_{PYEAR}-{PMONTH:0>2}'.format(**changes)


        # Update template deck.
        # Add a method to changes to update the wps deck:
        # changes.update_wps_deck()

        # Open template deck
        fin = open('../templates/getbdy_'+input_dic['INI_FORMAT']+'.deck', 'r')
        fout = open('{DECK_PATH}/getbdy_{INI_FORMAT}_{SYEAR}_{SMONTH:0>2}.deck'.format(**changes),'w')

        # Gives permissions: -rwxr--r-- to output file
        os.chmod(fout.name,stat.S_IRWXU)

        # Loop over the lines of the template csh script.
        for line in fin:
            line = string.Template(line).safe_substitute(changes)

            fout.write(line)

        # End of loop on the lines of the template csh script
        # Close files
        fout.close()
        fin.close()

        # Symlink to run directory for first script
        print("first: {first}".format(**changes))
        if changes['FIRST']:
            WRF_LIS_lib.create_symlink('{DECK_PATH}/getbdy_{INI_FORMAT}_{SYEAR}_{SMONTH:0>2}.deck'.format(**changes),input_dic['COUPLED_PATH'])

        # # If initial format is Grib, update the Makefile
        # fin = open('../Makefile', 'r')
        # fout = open('{DECK_PATH}/Makefile_{SYEAR}_{SMONTH:0>2}'.format(**changes),'w')

        # # Gives permissions: -rwxr--r-- to output file
        # os.chmod(fout.name,stat.S_IRWXU)

        # # Loop over the lines of the template csh script.
        # for line in fin:
        #     line = string.Template(line).safe_substitute(changes)

        #     fout.write(line)

        # # End of loop on the lines of the template csh script
        # # Close files
        # fout.close()
        # fin.close()

        # ## Symlink to run directory
        # #if changes['FIRST']:
        # #    WRF_LIS_lib.make_symlink('{DECK_PATH}/Makefile_{SYEAR}_{SMONTH:0>2}'.format(**changes),os.path.join(input_dic['COUPLED_PATH'],"Makefile"))


        # Get next month and next year
        month = int(changes['NMONTH'])
        year  = int(changes['NYEAR'])

        # Need to change first to false (or 0)
        changes['NOFIRST'] = '1'
        # NOSECOND is true (1) except if FIRST was true (1)
        changes['NOSECOND'] = 1 - changes['FIRST']
        changes['FIRST'] = 0
        changes['ISRESTART'] = '.true.'


    # End of the loop over the months to treat.
