#!/usr/bin/env python

import os, shutil
import os.path
import stat
import netCDF4 as nc
import string
import sys
import copy
import WRF_LIS_lib
                        
def prepare_WRF(input_dic):

    # Create link in run directory
    WRF_LIS_lib.create_symlink(input_dic["LISPARAMSDIR"],input_dic["COUPLED_PATH"])

    #-----------------------------------------------------------------------------------
    # Read the grid info from the geo_em.d01.nc file first.
    gridtype = input_dic['gridtype']

    # Read grid info for WRF
    tmp = WRF_LIS_lib.get_grid('WRF', gridtype, input_dic["GEO_PATH"], input_dic['max_dom'])
    input_dic.update(tmp)

    # Read grid info for LIS
    tmp = WRF_LIS_lib.get_grid('LIS', gridtype, input_dic["GEO_PATH"], input_dic['max_dom'])
    input_dic.update(tmp)

    # A few initialisations
    days_in_month = (31,28,31,30,31,30,31,31,30,31,30,31)

    start_year  = input_dic['start_year']
    start_month = input_dic['start_month']
    start_day = 1

    end_year    = input_dic['end_year']
    end_month   = input_dic['end_month']
    end_day = 1

    year = start_year
    month = start_month
    day = start_day

    # Get a new dictionary for the changes (cosmetic)
    changes = input_dic


    # Add some values
    changes['SDAY'] = '01'  
    changes['NDAY'] = '01'
    changes['ISRESTART'] = '.false.'
#    changes['RUN_DIR'] = os.getcwd()
    changes['FIRST'] = 1 #int(input_dic['first'])
    changes['NOFIRST'] = 1-changes['FIRST']

    if (input_dic['coupled']):
        # Get the names of the model output files
        tempo = changes['MODEL_OUTPUT_AR'].split(' ')
        if len(tempo) != changes['max_dom']:
            message="The number of filenames in MODEL_OUTPUT_AR must equal max_dom in input_scripts.txt"
            sys.exit(message)

        changes['MODELOUT_FILES_WRF_AR'] = ' '.join(map(os.path.basename,tempo))
        changes['MODELOUT_PATH_WRF'] = os.path.dirname(tempo[0])
        changes['OUTPUT_FILE_AR'] = ' '.join(map(repr,tempo))


        # Rewrite MODELOUT_FILES_WRF to decks.
        for i in range(len(tempo)):
            file1=os.path.basename(tempo[i])
            fsrc=open(os.path.join('../templates/',file1),'r')
            fdst=open(os.path.join(changes["DECK_PATH"],file1), 'w')

            for line in fsrc:
                fdst.write(line)

            # Close
            fsrc.close()
            fdst.close()
    # End coupled

    #-------------------------------------------------------------------------------
    #
    #                                                                TIME LOOP
    #
    #-------------------------------------------------------------------------------

    # Add non-time dependent info for lis.config
    changes['RUNMODE']  = "WRF coupling"
    changes['RESTART']  = 'restart'
    changes['REFHGT']  = 10 # Lowest model level


    # FORC_TXT is for the offline run, for the online run we need to point
    # to the same subdirectory as offline but use the wrf file.
    tmp = changes['FORC_TXT']
    changes['FORC_TXT']      = os.path.join(os.path.dirname(tmp),'forcing_variables_wrf_v341.txt')

    if changes['coupled']:
        changes['OLAP'] = int(float(changes['WRF_ts'])/60.)
    else:
        changes['OLAP'] = 00

    # Build FORC_LOC_DIR_AR needed for lis.config. It's only FORC_LOC_DIR repeated # of nests times.
    changes['FORC_LOC_DIR_AR'] = changes['max_dom'] * (changes['FORC_LOC_DIR'] + ' ')

    #set up loop to be over all months in the years mentioned
    while (year < end_year or (year == end_year and month <= end_month)):

        print('Prepare year/month:'+str(year)+'/'+str(month))
        # Is it February of a leap year?
        leapyear = int((month==2 and (((year%4==0) and (year%100 !=0)) or year%400 == 0) ))

        # Cut into subruns if needed
        days_per_run = (days_in_month[month-1]+1*leapyear)/input_dic['WRF_subruns']
        for nr in range(input_dic['WRF_subruns']):
            changes['PDAY'] = changes['SDAY']
            changes['SDAY'] = format(int(1 + nr*days_per_run), '02d')
            changes['NDAY'] = days_per_run + nr*days_per_run + 1
            if (nr == input_dic['WRF_subruns']-1):
                changes['NDAY'] = 1

            # Get previous month and previous year
            tmp=WRF_LIS_lib.set_pndate(year,month,changes['WRF_nmonths'], changes)
            changes.update(tmp)

            # Last day of month +1
            #changes['RDAY'] = str(days_in_month[month-1]+1 + (1*leapyear))

            # Get the year in the history files
            changes['HYEAR'] = str(year-1700)

            # Open template deck
            fin = open(os.path.join('../templates/','template_runwrf_raijin.deck'),'r')
            runfile = os.path.join(changes["DECK_PATH"],'runwrf_{SYEAR}_{SMONTH:0>2}_{SDAY:0>2}.deck'.format(**changes))
            fout = open(runfile,'w')

            # Gives permissions: -rwxr--r-- to output file
            os.chmod(fout.name,stat.S_IRWXU)

            # build the values of all flags

            # Transform SDAY and NDAY in strings
            changes['SDAY'] = '{SDAY:0>2}'.format(**changes)
            changes['NDAY'] = '{NDAY:0>2}'.format(**changes)

            # Restart interval: one per day
            changes['RES_WRF'] = 24*60
            changes['RES_LIS_AR'] = input_dic['max_dom']*(str(changes['RES_WRF']*60)+" ").strip()

            # Get name of the LIS config file
            changes['LIS_CONFIG'] = 'lis.config_wrf_{SYEAR}_{SMONTH:0>2}_{SDAY:0>2}'.format(**changes)

            # Loop over the lines of the template csh script.
            for line in fin:
                if (line.find("parent_time_step_ratio") != -1):
                    tmp = line.split("=")
                    tmp = tmp[1].strip().split(',')
                    tmp.remove('')
                    changes["PARENT_TIME_STEP_RATIO"] = list(map(int, tmp))

                line = string.Template(line).safe_substitute(changes)

                fout.write(line) 

            # End of loop on the lines of the template csh script

            # Close files
            fout.close()
            fin.close()

            # Link fout to run directory for first month only
            if ['FIRST']:
                WRF_LIS_lib.create_symlink(runfile,input_dic["COUPLED_PATH"])

            # Create lis.config_wrf if coupled run
            if (input_dic['coupled']):

                # Add time dependent entries
                changes['RESTART_FILE_AR'] = []
                for i in range(input_dic['max_dom']):
                    grid=str(i+1).rjust(2,'0')
                    changes['RESTART_FILE_AR'] = changes['RESTART_FILE_AR'] +  ['LIS_RST_{LSM}_{SYEAR}{SMONTH}{SDAY}0000.d'.format(**input_dic)
                                                                      +grid+'.nc'.format(**input_dic)]

                # Join in a string
                changes['RESTART_FILE_AR'] = ' '.join(changes['RESTART_FILE_AR'])

                # Time step: need the time step for all domains for LIS.
                changes['TIMESTEP'] = [int(input_dic['WRF_ts'])/changes['PARENT_TIME_STEP_RATIO'][i] for i in range(changes['max_dom'])]
                changes['TIMESTEP'] = [int(i) for i in changes['TIMESTEP']]
                changes['TIMESTEP'] = list(map(str,changes['TIMESTEP']))
                changes['TIMESTEP'] = [i+"ss" for i in changes['TIMESTEP']]
                changes['TIMESTEP'] = ' '.join(changes['TIMESTEP'])

                # Update the CO2 concentration 
                changes['CO2CONC'] = WRF_LIS_lib.get_co2_conc(input_dic,year)

                # Open the lis.config_wrf file
                fin = open(os.path.join('../templates/','template_lis.config'), 'r')
                fout = open(os.path.join(changes["DECK_PATH"],changes["LIS_CONFIG"]), 'w')

                for line in fin:
                    line = string.Template(line).safe_substitute(changes)

                    fout.write(line)
                # End of loop on lines of fin
                # Close files
                fout.close()
                fin.close()

             # Need to change nofirst to true (or 1) and first to false (or 0)
            changes['NOFIRST'] = '1'
            changes['FIRST'] = '0'
            changes['ISRESTART'] = '.true.'

        # End of loop on the sub-runs

        # Get next month and next year
        month = int(changes['NMONTH'])
        year     = int(changes['NYEAR'])

    # End of the loop over the months to treat.
    # Remove the tempo_transfer.csh file if it exists
    if os.path.isfile("tempo_transfer.csh"):
        os.remove("tempo_transfer.csh")

