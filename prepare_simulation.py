#!/usr/bin/python

#===================================================================================
#
# Script to prepare a WRF climate simulation, a LIS-WRF climate simulation or a 
# LIS simulation. This script prepares:
#
# - all namelist and configuration files.
# - the scripts to submit the simulations to the queue
# - the scripts will include the compilation of the programs before running the 
#   first month if asked for
#
#
# This script reads input in "input_scripts.txt" which must be present in the
# same directory as this script.
#===================================================================================

import os
import os.path
import string
import sys
import copy
import WRF_LIS_lib
import prepare_LIS
import prepare_WPS
import prepare_WRF

#-----------------------------------------------------------------------------------
# Read input file
#
section = 'LIS-WRF'

input_dic=WRF_LIS_lib.read_input(file='../templates/input_scripts.txt',section=section)
# Define which bits (LDT, LIS and WRF) need to be run
tmp = WRF_LIS_lib.whichsimul(input_dic['mode'])
input_dic.update(tmp)

# Define the input, output, run and code directories
tmp = WRF_LIS_lib.defdirs(input_dic)
input_dic.update(tmp)

# Save the value of first from the file
first = input_dic['first']

# The `mode` dictionary entry indicates which simulation we want.
if ( input_dic['mode'] == 'LIS' ): 
    prepare_LIS.LDT_prelis(input_dic)
    prepare_LIS.prepare_LIS(input_dic)

elif( input_dic['mode'] == 'WRF' ):
    prepare_WPS.prepare_WPS(input_dic)
    prepare_WRF.prepare_WRF(input_dic)

elif( input_dic['mode'] == 'NUWRF' ):
    prepare_LIS.LDT_prelis(input_dic)
    prepare_LIS.prepare_LIS(input_dic)
    prepare_LIS.LDT_postlis(input_dic)
    # Need to reset first. Think of doing things differently when redesigning?
    input_dic['first'] = first
    prepare_WPS.prepare_WPS(input_dic)
    prepare_WRF.prepare_WRF(input_dic)
else:
    print("mode is not defined correctly in 'input_scripts.txt' file")
    print("mode is %s in prepare_simulation.py"%input_dic['mode'])
    print("make sure mode is 'LIS', 'WRF' or 'NUWRF'")
