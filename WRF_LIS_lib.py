# Module of functions used by the program: prepare_LIS.py, prepare_WPS.py and prepare_WRF.py

import sys
import os
import stat
import configparser
import netCDF4 as nc
import datetime
import errno
import string
import pandas as pd

options_str = [ 'EMAIL', 'PROJECT', 'WRF_ts', 'LIS_ts', 'gridtype'  \
              , 'CODEDIR_ROOT', 'RUNDIR_ROOT', 'SCRIPTSDIR_ROOT'    \
              , 'MAKEINTER_DIR', 'LSM', 'OUTPUT_DIR'    \
              , 'MODEL_OUTPUT_AR', 'INI_FORMAT', 'DATA_DIR'         \
              , 'BASENAMES', 'FORC_LOC_DIR', 'FORC_TXT'             \
              , 'LISPARAMSDIR', 'mode', 'EXE_DIR','FORC_DATA_NAME' ]
options_int = [ 'start_month', 'start_year', 'end_month', 'end_year'\
              , 'len_spinup', 'WRF_subruns', 'max_dom', 'XPROC'     \
              , 'YPROC', 'NSOIL', 'LIS_nmonths', 'WRF_nmonths'      \
              , 'WPS_monthly_files'                                 \
              ]
options_flt = []
options_bool = [ 'first', 'compile' ]

# Lustre target paths for symbolic paths cannot be 60 characters (yes, really)
# Delete this once this bug in Lustre is fixed
CHECK_LUSTRE_PATH_LEN = True

#----------------------------------------------------------------------------------
# Function to read the input file
def read_input(file='../input_scripts.txt',section='LIS_offline'):
    "Read the input file for the scripts. Return a dictionary."

    # Return dictionary.
    input = {}

    cf = configparser.ConfigParser()
    cf.read(file)
    
    if ( not(cf.has_section(section))):
        message= 'Input file not complete!\n'
        message=message+'Must contain the sections:\n'
        message=message+'[DEFAULT]\n'
        message=message+'['+section+']'
        sys.exit(message)

            
    # Check that we have all the values and create a dictionary to store everything
    for option in options_str:
        tmp=option.strip()
                
        if not(cf.has_option(section, tmp)):
            message = 'Input file miss the {opt} option ' \
                      'for the {sec} section'.format(opt=tmp, sec=section)
            sys.exit(message)
                
        input[tmp] = cf.get(section,tmp)
                
    for option in options_int:
        tmp = option.strip()
        
        if not(cf.has_option(section, tmp)):
            message = 'Input file miss the {opt} option ' \
                      'for the {sec} section'.format(opt=tmp, sec=section)
            sys.exit(message)
                
        input[tmp] = cf.getint(section,tmp)

    for option in options_flt:
        tmp = option.strip()
        if not(cf.has_option(section, tmp)):
            message = 'Input file miss the {opt} option ' \
                      'for the {sec} section'.format(opt=tmp, sec=section)
            sys.exit(message)
                
        input[tmp] = cf.getfloat(section,tmp)

    for option in options_bool:
        tmp = option.strip()
        
        if not(cf.has_option(section, tmp)):
            message = 'Input file miss the {opt} option ' \
                      'for the {sec} section'.format(opt=tmp, sec=section)
            sys.exit(message)
                
        input[tmp] = cf.getboolean(section,tmp)
    
    # End all loops
     
    return input

#End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function to add nmonths to a date
def add_nmonths(sdate, nmonths):
    
    # How many years to add
    nyears  = nmonths/12
    
    # Add number of months minus the years to month and the number of years to year.
    lmonth = sdate.month + nmonths%12
    lyear  = sdate.year + nyears

    # Check the new month is below 12, else add +1 to year.
    if ( lmonth > 12):
        lmonth = lmonth - 12
        lyear  = lyear + 1
         
    return datetime.date(int(lyear),lmonth,1)

#-----------------------------------------------------------------------------------
# Function to set previous and next month and year.
def set_pndate(year, month, nmonths, dico):

    tmp={}
    #days_in_month = (31,28,31,30,31,30,31,31,30,31,30,31)

    # Create date from year and month
    sdate = datetime.date(year,month,1)

    # Create date from end_year and end_month
    edate = datetime.date(dico['end_year'],dico['end_month'],1)
    edate = add_nmonths(edate, 1)

    # Date of last day to run:
    # Get the date of the 1st day of the next month.
    ldate = add_nmonths(sdate, nmonths)

    # Check if ldate is later than edate. If so, truncate ldate to edate.
    if (ldate > edate):
        ldate = edate

    # Number of days to run
    timediff = ldate - sdate
    tmp['RUNDAYS'] = timediff.days

    # Get previous day date.
    pdate = sdate - datetime.timedelta(1)
    
    # Is it February of a leap year?
    leapyear = int(( month==2 and ( ( (year%4==0) and (year%100 !=0) ) or year%400 == 0 ) ))
    tmp['LEAP_YEAR']=leapyear

    # Format months, years and days as strings
    # Get month on a 2-digits string with leading 0 if necessary
    tmp['smonth'] = month
    tmp['SMONTH'] = '{:0>2}'.format(month)
    tmp['SYEAR'] = str(year)

    # Format of date variables.
    tmp['PMONTH'] = '{:0>2}'.format(pdate.month)
    tmp['PYEAR'] = str(pdate.year)
    tmp['PDAY'] = str(pdate.day)        
    
    tmp['NMONTH'] = '{:0>2}'.format(ldate.month)
    tmp['NYEAR']  = str(ldate.year)

    return tmp

#End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function to get the grid info necessary for LIS for a Lambert grid
def grid_LIS(GEOpath, max_dom, gridtype):
    geo = nc.Dataset(GEOpath+"/geo_em.d01.nc","r")

    tmp = {}
    if gridtype == "lambert" or gridtype == "mercator":
        tmp['TRUELAT1_AR'] = [str(geo.TRUELAT1)]
        tmp['STAND_LON_AR'] = [str(geo.STAND_LON)]
        tmp['RES_AR']      = [str(geo.DX/1000.)]   # M -> Km
        tmp['dom_type']    = "2"
        if gridtype == "lambert":
            tmp['TRUELAT2_AR'] = [str(geo.TRUELAT2)]
            tmp['dom_type'] = "3"
    elif gridtype == "lat-lon":
        lon=geo.variables['XLONG_M']
        tmp['RESX_AR'] = [str(lon[0,0,1]-lon[0,0,0])]
        lat=geo.variables['XLAT_M']
        tmp['RESY_AR'] = [str(lat[0,1,0]-lat[0,0,0])]
        tmp['URLAT_AR'] = [str(geo.corner_lats[2])]
        tmp['URLON_AR'] = [str(geo.corner_lons[2])]
        tmp['dom_type'] = "1"
    elif gridtype == "polar":
        print("Polar grids are not implemented yet for LIS.")
        print("You need to have the grid values written in the lis.config")
        tmp['dom_type'] = "5"
        #tmp['ORIENT_AR'] =
        #tmp['TRUELAT1_AR'] = [str(geo.TRUELAT1)]
        #tmp['STAND_LON_AR'] = [str(geo.STAND_LON)]
        #tmp['RES_AR'] = [str(geo.DX)]
        
    tmp['LLLAT_AR']    = [str(geo.corner_lats[0])]
    tmp['LLLON_AR']    = [str(geo.corner_lons[0])]
    tmp['XSIZE_AR']    = [str(geo.__dict__['WEST-EAST_GRID_DIMENSION'] - 1)]
    tmp['YSIZE_AR']    = [str(geo.__dict__['SOUTH-NORTH_GRID_DIMENSION'] - 1)]

    geo.close()

    for i in range(max_dom-1):
        grid=str(i+2).rjust(2,'0')

        geo = nc.Dataset(GEOpath+"/geo_em.d"+grid+".nc","r")

        if gridtype == "lambert" or gridtype == "mercator":
            tmp['TRUELAT1_AR'] = tmp['TRUELAT1_AR'] + [str(geo.TRUELAT1)]
            tmp['STAND_LON_AR'] = tmp['STAND_LON_AR'] + [str(geo.STAND_LON)]
            tmp['RES_AR']      = tmp['RES_AR'] + [str(geo.DX/1000.)]   # M -> Km
            if gridtype == "lambert":
                tmp['TRUELAT2_AR'] = tmp['TRUELAT2_AR'] + [str(geo.TRUELAT2)]
        elif gridtype == "lat-lon":
            lon=geo.variables['XLONG_M']
            tmp['RESX_AR'] = tmp['RESX_AR'] + [str(lon[0,0,1]-lon[0,0,0])]
            lat=geo.variables['XLAT_M']
            tmp['RESY_AR'] = tmp['RESY_AR'] + [str(lat[0,1,0]-lat[0,0,0])]
            tmp['URLAT_AR'] = tmp['URLAT_AR'] + [str(geo.corner_lats[2])]
            tmp['URLON_AR'] = tmp['URLON_AR'] + [str(geo.corner_lons[2])]
            
        tmp['LLLAT_AR']    = tmp['LLLAT_AR'] + [str(geo.corner_lats[0])]
        tmp['LLLON_AR']    = tmp['LLLON_AR'] + [str(geo.corner_lons[0])]
        tmp['XSIZE_AR']    = tmp['XSIZE_AR'] + [str(geo.__dict__['WEST-EAST_GRID_DIMENSION'] - 1)]
        tmp['YSIZE_AR']    = tmp['YSIZE_AR'] + [str(geo.__dict__['SOUTH-NORTH_GRID_DIMENSION'] - 1)]
            
        geo.close()
    
    # Transform the lists in strings
    if gridtype == "lambert" or gridtype == "mercator":
        tmp['TRUELAT1_AR'] = ' '.join(tmp['TRUELAT1_AR'])
        tmp['STAND_LON_AR'] = ' '.join(tmp['STAND_LON_AR'])
        tmp['RES_AR']      = ' '.join(tmp['RES_AR'])
        if gridtype == "lambert":
            tmp['TRUELAT2_AR'] = ' '.join(tmp['TRUELAT2_AR'])
    if gridtype == "lat-lon":
        tmp['RESX_AR'] = ' '.join(tmp['RESX_AR'])
        tmp['RESY_AR'] = ' '.join(tmp['RESY_AR'])
        tmp['URLAT_AR'] = ' '.join(tmp['URLAT_AR'])
        tmp['URLON_AR'] = ' '.join(tmp['URLON_AR'])
    
    tmp['LLLAT_AR']    = ' '.join(tmp['LLLAT_AR'])
    tmp['LLLON_AR']    = ' '.join(tmp['LLLON_AR'])
    tmp['XSIZE_AR']    = ' '.join(tmp['XSIZE_AR'])
    tmp['YSIZE_AR']    = ' '.join(tmp['YSIZE_AR'])

    return tmp
# End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function to get the grid info necessary for WRF
def grid_WRF(GEOpath, max_dom, gridtype):
    geo = nc.Dataset(GEOpath+"/geo_em.d01.nc","r")

    tmp = {}

    if gridtype == "lambert":
        tmp['TRUELAT1']             =  str(geo.TRUELAT1)
        tmp['TRUELAT2']             =  str(geo.TRUELAT2)
        tmp['STAND_LON']            =  str(geo.STAND_LON)
        tmp['DX']                   =  str(geo.DX)
        tmp['DY']                   =  str(geo.DY)
        tmp['POLE_LAT']             = str(geo.POLE_LAT)
        tmp['POLE_LON']             = str(geo.POLE_LON)
    elif gridtype == "lat-lon":
        tmp['POLE_LAT']             = str(geo.POLE_LAT)
        tmp['POLE_LON']             = str(geo.POLE_LON)
        tmp['STAND_LON']            = str(geo.STAND_LON)
        lon=geo.variables['XLONG_M']
        tmp['DX']                   =  str(lon[0,0,1]-lon[0,0,0])
        lat=geo.variables['XLAT_M']
        tmp['DY']                   =  str(lat[0,1,0]-lat[0,0,0])
    elif gridtype == "mercator":
        tmp['TRUELAT1']             =  str(geo.TRUELAT1)
        tmp['DX']                   =  str(geo.DX)
        tmp['DY']                   =  str(geo.DY)
    elif gridtype == "polar":
        tmp['TRUELAT1']             =  str(geo.TRUELAT1)
        tmp['STAND_LON']            =  str(geo.STAND_LON)
        tmp['DX']                   =  str(geo.DX)
        tmp['DY']                   =  str(geo.DY)
    else:
        print("Only the lambert, lat-lon, mercator and polar grids are supported for WRF.")
        print("Hard-wire your grid data if you want to use an other projection.")
    
    tmp['REF_LAT']              =  str(geo.CEN_LAT)
    tmp['REF_LON']              =  str(geo.CEN_LON)
    tmp['DX_AR']                = [str(geo.DX)]
    tmp['DY_AR']                = [str(geo.DY)]
    tmp['GRID_ID_AR']           = [str(geo.grid_id)]
    tmp['PARENT_ID_AR']         = [str(geo.parent_id)]
    tmp['PARENT_GRID_RATIO_AR'] = [str(geo.parent_grid_ratio)]
    tmp['I_PARENT_START_AR']    = [str(geo.i_parent_start)]
    tmp['J_PARENT_START_AR']    = [str(geo.j_parent_start)]
    tmp['E_WE_AR']              = [str(geo.__dict__['WEST-EAST_GRID_DIMENSION'])]
    tmp['E_SN_AR']              = [str(geo.__dict__['SOUTH-NORTH_GRID_DIMENSION'])]

    geo.close()

    for i in range(max_dom-1):
        grid=str(i+2).rjust(2,'0')

        geo = nc.Dataset(GEOpath+"/geo_em.d"+grid+".nc","r")
        
        tmp['GRID_ID_AR'] = tmp['GRID_ID_AR'] + [str(geo.grid_id)]
        tmp['PARENT_ID_AR'] = tmp['PARENT_ID_AR'] + [str(geo.parent_id)]
        tmp['PARENT_GRID_RATIO_AR'] = tmp['PARENT_GRID_RATIO_AR'] + [str(geo.parent_grid_ratio)]
        tmp['I_PARENT_START_AR'] = tmp['I_PARENT_START_AR'] + [str(geo.i_parent_start)]
        tmp['J_PARENT_START_AR'] = tmp['J_PARENT_START_AR'] + [str(geo.j_parent_start)]
        tmp['E_WE_AR']    = tmp['E_WE_AR'] + [str(geo.__dict__['WEST-EAST_GRID_DIMENSION'])]
        tmp['E_SN_AR']    = tmp['E_SN_AR'] + [str(geo.__dict__['SOUTH-NORTH_GRID_DIMENSION'])]
        tmp['DX_AR'] = tmp['DX_AR'] + [str(geo.DX)]
        tmp['DY_AR'] = tmp['DY_AR'] + [str(geo.DY)]

            
        geo.close()
    
    # Transform the lists in strings
    tmp['GRID_ID_AR']           = ', '.join(tmp['GRID_ID_AR'])
    tmp['PARENT_ID_AR']         = ', '.join(tmp['PARENT_ID_AR'])
    tmp['PARENT_GRID_RATIO_AR'] = ', '.join(tmp['PARENT_GRID_RATIO_AR'])
    tmp['I_PARENT_START_AR']    = ', '.join(tmp['I_PARENT_START_AR'])
    tmp['J_PARENT_START_AR']    = ', '.join(tmp['J_PARENT_START_AR'])
    tmp['E_WE_AR']              = ', '.join(tmp['E_WE_AR'])
    tmp['E_SN_AR']              = ', '.join(tmp['E_SN_AR'])
    tmp['DX_AR']                = ', '.join(tmp['DX_AR'])
    tmp['DY_AR']                = ', '.join(tmp['DY_AR'])

    return tmp
# End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Generic function call to get grid info for LIS
def get_grid(model, gridtype, GEOpath, max_dom):
    if (model == "LIS"):
        tmp = grid_LIS(GEOpath, max_dom, gridtype)
    elif (model == "WRF"):
        tmp = grid_WRF(GEOpath, max_dom, gridtype)
    else:
        sys.exit("The model must be LIS or WRF")

    return tmp
# End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Symbolic link 
def make_symlink(src_path, lnk_path):
    """Safely create a symbolic link to an input field."""

    # Check for Lustre 60-character symbolic link path bug
    if CHECK_LUSTRE_PATH_LEN:
        src_path = patch_lustre_path(src_path)
        lnk_path = patch_lustre_path(lnk_path)

    try:
        os.symlink(src_path, lnk_path)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            print('Path to link to {p}'.format(p=src_path))
            print('Link name {l}'.format(l=lnk_path))
            raise
        elif not os.path.islink(lnk_path):
            # Warn the user, but do not interrupt the job
            print("Warning: Cannot create symbolic link to {p}; a file named "
                  "{f} already exists.".format(p=src_path, f=lnk_path))
        else:
            # Overwrite any existing symbolic link
            if os.path.realpath(lnk_path) != src_path:
                os.remove(lnk_path)
                os.symlink(src_path, lnk_path)

#End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function to create a link
def create_symlink(src_path, lnk_path):
    """ Get 2 absolute paths, create a link in lnk_path named as the last directory
        path in src_path"""

    lastbit = os.path.basename(os.path.normpath(src_path))
    linkpath=os.path.join(lnk_path,lastbit)
    make_symlink(src_path,linkpath)
#End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function
def patch_lustre_path(f_path):
    """Patch any 60-character pathnames, to avoid a current Lustre bug."""

    if CHECK_LUSTRE_PATH_LEN and len(f_path) == 60:
        if os.path.isabs(f_path):
            f_path = '/.' + f_path
        else:
            f_path = './' + f_path

    return f_path
#End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function 
def make_directories(tmp):
    '''Create all directories listed in tmp if they don't already exist'''
    
    for dirname in tmp:
        print("Creating directory: {} if not existing".format(tmp[dirname]))
        try:
            os.makedirs(tmp[dirname])
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                print('Impossible to create the directory {}'.format(tmp[dirname]))
                raise
 #End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function       
def defdirs(input_dic):
    '''Define all the input, output, run and code directories from the root directories in the input file'''
    
    tmp1={}
    tmp={}

    # Define the paths first.
    # DECK directory
    tmp1['DECK_DIR'] = 'decks'
    tmp['DECK_PATH'] = os.path.join(input_dic['SCRIPTSDIR_ROOT'],tmp1['DECK_DIR'])

    tmp1['GEO_DIR'] = '.'
    tmp['GEO_PATH'] = os.path.join(input_dic['SCRIPTSDIR_ROOT'],tmp1['GEO_DIR'])

    # LDT prelis run directory if coupled run
    if input_dic['LDTprelis']:
        tmp1['LDTprelis_DIR'] = 'LDTprelis'
        tmp['LDTprelis_PATH'] = os.path.join(input_dic['RUNDIR_ROOT'],tmp1['LDTprelis_DIR'])
        

    # LIS offline directories
    if input_dic['LIS_simul']:
        tmp1['LIS_DIR'] = 'LIS_offline'
        tmp['LIS_PATH'] = os.path.join(input_dic['RUNDIR_ROOT'],tmp1['LIS_DIR'])

        # Need to search for all occurences of SCRIPTS_LISOUT and replace
        tmp1['LISOUT_DIR'] = 'LIS_output'
        tmp['LISOUT_PATH'] = os.path.join(input_dic['SCRIPTSDIR_ROOT'],tmp1['LISOUT_DIR'])
    
    # LDT postlis directories
    if input_dic['LDTpostlis']:
        tmp1['LDTpostlis_DIR'] = 'LDTpostlis'
        tmp['LDTpostlis_PATH'] = os.path.join(input_dic['RUNDIR_ROOT'],tmp1['LDTpostlis_DIR'])
        tmp1['BDYDATA_DIR'] = 'bdy_data'
        tmp['BDYDATA_PATH'] = os.path.join(input_dic['SCRIPTSDIR_ROOT'],tmp1['BDYDATA_DIR'])

    if input_dic['WRF_simul']:
        tmp1['COUPLED_DIR'] = 'coupled_run'
        tmp['COUPLED_PATH'] = os.path.join(input_dic['RUNDIR_ROOT'],tmp1['COUPLED_DIR'])
        tmp1['WRFOUT_DIR'] = 'WRF_output'
        tmp['WRFOUT_PATH'] = os.path.join(input_dic['SCRIPTSDIR_ROOT'],tmp1['WRFOUT_DIR'])
        tmp1['BDYDATA_DIR'] = 'bdy_data'
        tmp['BDYDATA_PATH'] = os.path.join(input_dic['SCRIPTSDIR_ROOT'],tmp1['BDYDATA_DIR'])

    # Create directories that don't already exist
    make_directories(tmp)

    tmp.update(tmp1)
    return tmp
#End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function
def whichsimul(mode):
    '''Define logical to know which bits (LDT, LIS and/or WRF) need to run depending on the value of mode'''
    
    # Check mode value is valid
    tmp = {                  \
        'coupled': False     \
       ,'LDTprelis': False  \
       ,'LDTpostlis': False \
       ,'LIS_simul': False  \
       ,'WRF_simul': False  }

    if ( any(x == mode.lower() for x in ['lis', 'wrf', 'nuwrf'])):

        if ( mode.lower() == 'nuwrf' ):
            for k in tmp:
                tmp[k] = True 

        elif ( mode.lower() == 'lis' ):
            tmp['LDTprelis'] = True
            tmp['LIS_simul'] = True

        elif ( mode.lower() == 'wrf' ):
            tmp['WRF_simul'] = True

    else:
        message = 'mode in input_scripts.txt is not a valid value\n'
        message+= 'It should be LIS, WRF or NUWRF\n'
        message+= 'mode currently is '+mode
        sys.exit(message)
    

    return tmp
#End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function
def files_to_update(step, input_dic):
    '''Create a list of template files that need to be updated for the config with the 
    corresponding output file'''

    if step == 'LDTprelis':
        tempfiles=['template_ldt.config.prelis', 'template_run_LDTprelis.sh']
        fileslist=[(x,x.replace('template_',''),'') for x in tempfiles]

    if step == 'LDTpostlis':
        tempfiles=['template_ldt.config.postlis', 'template_run_LDTpostlis.sh']
        fileslist=[(x,x.replace('template_',''),'') for x in tempfiles]

    if step == 'LIS':
        tempfiles=['template_run_LIS.csh', 'template_lis.config']
        fileslist=[(tempfiles[0],'run_LIS_{SYEAR}_{SMONTH}'.format(**input_dic),'x'), \
                    (tempfiles[1],'{LIS_CONFIG}'.format(**input_dic),'')]
        print("For LIS files are:")
        print(fileslist)
    
    update_template(fileslist, input_dic)
#End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function       
def update_template(fileslist, input_dic):
    '''Update the template file to create the real case file'''

    for filein, fileout, isexe in fileslist:
        print("Going to update the file "+os.path.join(input_dic['DECK_PATH'],fileout))
        tin = open('../templates/'+filein,'r')
        tout = open(os.path.join(input_dic['DECK_PATH'],fileout),'w')

        for line in tin:
            #Update the variable fields to build the correct file
            line = string.Template(line).safe_substitute(input_dic)

            tout.write(line)

        # If isexe == 'x' make outfile executable
        if isexe == 'x':
            os.fchmod(tout.fileno(),stat.S_IRWXU)
        
        tout.close()
        tin.close()
#End function
#-----------------------------------------------------------------------------------

#-----------------------------------------------------------------------------------
# Function to retrieve CO2 concentration for a 
def get_co2_conc(input_dic,year):
    '''Retrieve the appropriate CO2 concentration value from file'''

    co2filepath = os.path.join(input_dic['CODEDIR_ROOT'],'WRFV3/run')
    df = pd.read_fwf(co2filepath+'/CAMtr_volume_mixing_ratio.RCP4.5',widths=[4,2,7,4,7,4,7,4,7,4,7], skiprows=(0,1))
    df.columns = ["year", "a", "co2", "b", "n20", "c", "ch4", "d", "cfc11", "e", "cfc12"]
    ghg = df.loc[ df["year"] == year ]
    co2conc = ghg["co2"].values[0]
    return co2conc
